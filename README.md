# docker-images

consortium of docker images ...
... mostly for woodpecker usage

- [Dockerfile.pre-commit](Dockerfile.pre-commit): https://hub.docker.com/r/a6543/pre-commit
- [Dockerfile.ansible-lint](Dockerfile.ansible-lint): https://hub.docker.com/r/a6543/ansible-lint
- [Dockerfile.golang_just](Dockerfile.golang_just): https://hub.docker.com/r/a6543/golang_just
- [Dockerfile.print_env](Dockerfile.print_env): [`docker pull codeberg.org/6543/docker-images/print_env`](https://codeberg.org/6543/-/packages/container/docker-images%252Fprint_env)
- [Dockerfile.hello](Dockerfile.hello): [`docker pull codeberg.org/6543/hello`](https://codeberg.org/6543/-/packages/container/hello)
- editorconfig-checker: [docker pull codeberg.org/6543/docker-images/editorconfig-checker`](https://codeberg.org/6543/-/packages/container/docker-images%252Feditorconfig-checker)
